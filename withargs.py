#!/usr/bin/env python3


class decorator_class_with_args():
    def __init__(self, *args, **kwargs):
        self.dec_args = args
        self.dec_kwargs = kwargs

    def __call__(self, f):
        def wrapper(*args, **kwargs):
            print('class decorator with arguments')
            print(f"Decorator arguments: {args}, {kwargs}")
            f(*args, **kwargs)
        return wrapper


@decorator_class_with_args('arg', key='kwarg')
def some_class_decorator_with_args(*args, **kwargs):
    print(f"Function arguments: {args}, {kwargs}")


def decorator_function_with_arguments(*dec_args, **dec_kwargs):
    def wrap(f):
        def wrapped_f(*args, **kwargs):
            print('func decorator with arguments')
            print(f"Decorator arguments: {dec_args}, {dec_kwargs}")
            f(*args, **kwargs)
        return wrapped_f
    return wrap


@decorator_function_with_arguments('arg', key='kwarg')
def some_func_decorator_with_args(*args, **kwargs):
    print(f"Function arguments: {args}, {kwargs}")


if __name__ == "__main__":
    some_class_decorator_with_args("args", "passed", "to", "func", key="kwarg")
    some_func_decorator_with_args("args", "passed", "to", "func", key="kwarg")
