# PBJing a PBJ
## Python Decorators at IndyPy

### Jun 12, 2018

- slides.html is a slideshow

```sh
clone git@gitlab.com:pinkhatbeard/pbjpbj.git
cd pbjpbj
python3 -m http.server
```
```sh
>>> Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...
```
- slides will be available at `localhost:8000`
